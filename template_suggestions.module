<?php

/**
 * Implements hook_preprocess_block().
 * 
 * Adds additional theme suggestions for block templates.
 */
function template_suggestions_preprocess_block(&$variables) {
  if (isset($variables['elements']['#block'])) {
    if (isset($variables['is_front']) && $variables['is_front'] == 1)
    {
      foreach ($variables['theme_hook_suggestions'] as $suggestion)
      {
        $variables['theme_hook_suggestions'][] = $suggestion . '__front';
      }
    }
  }

  // If language is enabled, then allow for specific language templates for
  // each template that already exists, ie page__user now has page__user__en
  _template_suggestions_add_language_templates($variables);
}


/**
 * Implements hook_preprocess_page().
 * 
 * Adds additional theme suggestions for page templates.
 */
function template_suggestions_preprocess_page(&$variables) {
  // If views is installed and we are a view ...
  if (module_exists('views') && isset($variables['page']['#views_contextual_links_info'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['page']['#views_contextual_links_info']['views_ui']['view']->name;
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['page']['#views_contextual_links_info']['views_ui']['view_display_name'];
  }

  if (isset($variables['node']) && !empty($variables['node'])) {
    $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
  }

  // If language is enabled, then allow for specific language templates for
  // each template that already exists, ie page__user now has page__user__en
  _template_suggestions_add_language_templates($variables);
}


/**
 * Implements hook_preprocess_node().
 * 
 * Adds additional theme suggestions for node templates.
 */
function template_suggestions_preprocess_node(&$variables) {
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['nid'] . '__' . $variables['view_mode'];

  // If language is enabled, then allow for specific language templates for
  // each template that already exists, ie page__user now has page__user__en
  _template_suggestions_add_language_templates($variables);
}


/**
 * Implements hook_preprocess_comment().
 * 
 * Adds additional theme suggestions for comment templates.
 */
function template_suggestions_preprocess_comment(&$variables) {
  $variables['theme_hook_suggestions'][] = str_replace("_", "__", $variables['comment']->node_type);

  // If language is enabled, then allow for specific language templates for
  // each template that already exists, ie page__user now has page__user__en
  _template_suggestions_add_language_templates($variables);
}


/**
 * This function loops through all existing theme_hook_suggestions and adds an
 * additional template using the language if set, maintaining order and adding
 * the language immediately after the template.
 */
function _template_suggestions_add_language_templates(&$variables) {
  if (isset($variables['language']->language)) {
    $new_templates = array();
    for ($x=0; $x<count($variables['theme_hook_suggestions']); $x++) {
      $new_templates[] = $variables['theme_hook_suggestions'][$x];
      $new_templates[] = $variables['theme_hook_suggestions'][$x] . '__' . $variables['language']->language;
    }
    
    $variables['theme_hook_suggestions'] = $new_templates;
  }
}

